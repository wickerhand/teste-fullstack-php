<?php
	if($_SERVER['SERVER_NAME'] == '127.0.0.1' || $_SERVER['SERVER_NAME'] == 'localhost')
	{
		$protocol = 'http://';
	}
	else
	{
		$protocol = 'https://';
	}
	define('root_path', $protocol.$_SERVER['SERVER_NAME'].'/mvc/');
	define('INCLUDE_PATH', root_path);
	define('INCLUDE_PATH_FULL', root_path.'Views/pages/');
	define('public_path', root_path.'public/');
	class Application
	{
		
		public function executar(){
			$url = isset($_GET['url']) ? explode('/',$_GET['url'])[0] : 'Home';
			$url = ucfirst($url);
			$urlComposta = explode('-', $url);
			
			if(count($urlComposta) > 1)
			{	
				$url = '';
				foreach($urlComposta as $item)
				{
					
					$url .= ucfirst($item);
				}
			}
			
			$url.="Controller";
			
			if(file_exists('Controllers/'.$url.'.php')){
				$className = 'Controllers\\'.$url;
				$controler = new $className;
				$controler->executar();
			}else{
				die("Não existe esse controlador!");
			}
		}
	}
	
?>