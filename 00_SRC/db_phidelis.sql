-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 09-Fev-2021 às 18:35
-- Versão do servidor: 10.4.14-MariaDB
-- versão do PHP: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `db_phidelis`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `estados`
--

CREATE TABLE `estados` (
  `id_estado` int(11) NOT NULL,
  `estado` varchar(155) COLLATE utf8_unicode_ci NOT NULL,
  `uf` char(2) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `estados`
--

INSERT INTO `estados` (`id_estado`, `estado`, `uf`) VALUES
(1, 'Espírito Santo', 'ES'),
(2, 'Rio de Janeiro', 'RJ'),
(4, 'São Paulo', 'SP'),
(6, 'Minas Gerais', 'MG');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imagens_slider`
--

CREATE TABLE `imagens_slider` (
  `id_imagem_slider` int(11) NOT NULL,
  `nome_imagem` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `texto_banner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_slider` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `imagens_slider`
--

INSERT INTO `imagens_slider` (`id_imagem_slider`, `nome_imagem`, `texto_banner`, `id_slider`) VALUES
(1, 'img-hero.jpg', 'Esse é um banner de teste', 5),
(3, 'img-hero-1.jpg', 'Mais uma imagem para compor o banner', 5),
(4, 'img-hero-2.jpg', 'Ultima imagem para compor o banner', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `publicacoes`
--

CREATE TABLE `publicacoes` (
  `id_publicacao` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `iframe_video` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_estado` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `publicacoes`
--

INSERT INTO `publicacoes` (`id_publicacao`, `titulo`, `descricao`, `iframe_video`, `id_estado`, `id_usuario`) VALUES
(2, 'Conheça o convento da penha ', '<p>A pedra azul é um lugar encantador pertencente ao município de Domingos Martins, com vistas exuberantes e ar apixonante!</p><p>Além da pedra, existe a rota do lagarto, com lindas paisagens ao redor da pedra.</p><p></p><ul><li>- elemento 1 da lista</li><li>- elemento 2 da lista</li><li>- elemento 3 da lista</li></ul><p></p><p><img src=\"https://assets.folhavitoria.com.br/images/097ebcf0-0e29-0138-8e0f-0a58a9feac2a--minified.jpg\" alt=\"Convento da penha\"></p><p>E aqui continua o texto...</p>', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Zme_NZv9d2A\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sliders`
--

CREATE TABLE `sliders` (
  `id_slider` int(11) NOT NULL,
  `titulo_slider` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sliders`
--

INSERT INTO `sliders` (`id_slider`, `titulo_slider`) VALUES
(5, 'Home');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'A = Ativo\nB = Bloqueado\nE = Excluído (Exclusão lógica para manter publicações feitas por ele)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario`, `email`, `senha`, `status`) VALUES
(1, 'Wicker Hand', 'wickerhand@gmail.com', '$2y$10$2eiztH0m0F1bnTgkp9dLt.JotNdh/SKEKQZ5mr9lK2Ygq3cQGhFUS', 'A'),
(2, 'Nícolas Aigner', 'nicolas@gmail.com', '$2y$10$1r4wKqYi9IDkYSLuTosmM.hFgPYFuqcEw25ISWPIgDn2XJ8l9UCkG', 'A'),
(3, 'Juliana Fiorot', 'juteste@teste.com', '$2y$10$Qg9QRANu/H2g6avZ3NPXVO9sZluJN0S2KQMlFJQiSwDa80i7Jnh4K', 'A');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id_estado`);

--
-- Índices para tabela `imagens_slider`
--
ALTER TABLE `imagens_slider`
  ADD PRIMARY KEY (`id_imagem_slider`),
  ADD KEY `fk_imagens_slider_sliders1_idx` (`id_slider`);

--
-- Índices para tabela `publicacoes`
--
ALTER TABLE `publicacoes`
  ADD PRIMARY KEY (`id_publicacao`),
  ADD KEY `fk_publicacoes_estados_idx` (`id_estado`),
  ADD KEY `fk_publicacoes_usuarios1_idx` (`id_usuario`);

--
-- Índices para tabela `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id_slider`);

--
-- Índices para tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `estados`
--
ALTER TABLE `estados`
  MODIFY `id_estado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `imagens_slider`
--
ALTER TABLE `imagens_slider`
  MODIFY `id_imagem_slider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `publicacoes`
--
ALTER TABLE `publicacoes`
  MODIFY `id_publicacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id_slider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `imagens_slider`
--
ALTER TABLE `imagens_slider`
  ADD CONSTRAINT `fk_imagens_slider_sliders1` FOREIGN KEY (`id_slider`) REFERENCES `sliders` (`id_slider`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `publicacoes`
--
ALTER TABLE `publicacoes`
  ADD CONSTRAINT `fk_publicacoes_estados` FOREIGN KEY (`id_estado`) REFERENCES `estados` (`id_estado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_publicacoes_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
