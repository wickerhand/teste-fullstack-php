jQuery(document).ready(function()
{
    returnPage();
    validarUsuario();
    logout();
    cadastrarItens();
    editarItens();
    openModalExclude();
    excluirItens();
    alterarSenha();
    editarSenha();
    inputFileValue();
    eviarAnexo();
    excluirAnexo();
    activeSlider();
    boxEstadosSlider();
    EstadoFiltro();
    pesquisaPublicacaoByEstado();
    openModalCadastro();
    openModalAlterarSenha();
    cadastrarUsuarios();
    
    if($('select').length)
    {
        $('select').selectmenu();
    }

    if($('#html_text').length)
    {
        $('#html_text').trumbowyg();
    }
});

function returnPage()
{
    $('.return-button').click(function(e)
    {
        e.preventDefault();

        history.back();
    })
}

function validarUsuario()
{
    $('#login_form').submit(function(e)
    {
        e.preventDefault();
        var count = 0;
        const _this = $(this);
        const messageItem = $('.msg');
        const messageParent = messageItem.parent();

        if(messageItem.hasClass('alert'))
        {
            messageItem.removeClass('alert');
        }
        else if(messageItem.hasClass('success'))
        {
            messageItem.removeClass('success');
        }
        messageItem.html('Enviando');
        messageParent.addClass('loading');

        $( ".campoObrigatorio", _this ).each(function() {
            if($(this).val() == '')
            {
                $(this).parents('.wrapper-input').addClass('alert');
                count++;
            }
        });

        if(count > 0 ){
            messageParent.slideDown();
            messageParent.addClass('alert');
            messageItem.html('Preencha os campos!');
            setTimeout(function(){
                messageParent.slideUp();
            }, 5000);
        }
        else if(!IsEmail($('.vEmail').val()))
        {
            messageParent.slideDown();
            messageParent.addClass('alert');
            messageItem.html('Preencha corretamente o E-mail');
            setTimeout(function(){
                messageParent.slideUp();
            }, 5000);
        }
        else {
            const _data = $(this).serialize();
            $.ajax({
                type: 'POST',
                url: 'login',
                data: _data,
                beforeSend: function(){
                    $('button', _this).html('Enviando ...');
                    $('button', _this).attr('disabled', 'disabled');
                },
                complete : function()
                {
                    $('button', _this).html('Salvar');
                    $('button', _this).attr('disabled', false);
                },
                success: function (response) {
                    const _response = JSON.parse(response);
                    if(_response.status == true)
                    {
                        window.location = '/mvc/dashboard';
                    }
                    else {
                        messageParent.slideDown();
                        messageItem.removeClass('success');
                        messageParent.addClass('alert');
                        messageItem.html(_response.response);
                        setTimeout(function(){
                            messageParent.slideUp();
                        }, 5000);
                    }
                },
                error: function (response) {
                    messageItem.addClass('alert');
                    messageItem.removeClass('loading');

                },
            });
        }
    });
}

function cadastrarItens()
{
    $('#form_cadastrar').submit(function(e)
    {
        e.preventDefault();
        var count = 0;
        const _this = $(this);
        const messageItem = $('.msg');
        const messageParent = messageItem.parent();
        const _url = $(this).attr('data-form');
        const _main = $(this).attr('data-main');

        if(messageItem.hasClass('alert'))
        {
            messageItem.removeClass('alert');
        }
        else if(messageItem.hasClass('success'))
        {
            messageItem.removeClass('success');
        }
        messageItem.html('Enviando');
        messageParent.addClass('loading');

        $( ".campoObrigatorio", _this ).each(function() {
            if($(this).val() == '')
            {
                $(this).parents('.wrapper-input').addClass('alert');
                count++;
            }
        });

        if(count > 0 ){
            messageParent.slideDown();
            messageParent.addClass('alert');
            messageItem.html('Preencha os campos!');
            setTimeout(function(){
                messageParent.slideUp();
            }, 5000);
        }
        else if($('.vEmail').length && !IsEmail($('.vEmail').val()))
        {
            messageParent.slideDown();
            messageParent.addClass('alert');
            messageItem.html('Preencha corretamente o E-mail');
            setTimeout(function(){
                messageParent.slideUp();
            }, 5000);
        }
        else {
            const _data = $(this).serialize();
           
            $.ajax({
                type: 'POST',
                url: _url,
                data: _data,
                beforeSend: function(){
                    $('button', _this).html('Enviando ...');
                    $('button', _this).attr('disabled', 'disabled');
                },
                complete : function()
                {
                    $('button', _this).html('Salvar');
                    $('button', _this).attr('disabled', false);
                },
                success: function (response) {
                    const _response = JSON.parse(response);
                    if(_response.status == true)
                    {
                        window.location = '/mvc/editar-'+_main.toLowerCase()+'?id='+_response.id;
                    }
                    else {
                        messageParent.slideDown();
                        messageItem.removeClass('success');
                        messageParent.addClass('alert');
                        messageItem.html(_response.response);
                        setTimeout(function(){
                            messageParent.slideUp();
                        }, 5000);
                    }
                },
                error: function (response) {
                    messageItem.addClass('alert');
                    messageItem.removeClass('loading');

                },
            });
        }
    });
}

function cadastrarUsuarios()
{
    $('#cadastrar_usuario').submit(function(e)
    {
        e.preventDefault();
        var count = 0;
        const _this = $(this);
        const messageItem = $('.msg');
        const messageParent = messageItem.parent();

        if(messageItem.hasClass('alert'))
        {
            messageItem.removeClass('alert');
        }
        else if(messageItem.hasClass('success'))
        {
            messageItem.removeClass('success');
        }
        messageItem.html('Enviando');
        messageParent.addClass('loading');

        $( ".campoObrigatorio", _this ).each(function() {
            if($(this).val() == '')
            {
                $(this).parents('.wrapper-input').addClass('alert');
                count++;
            }
        });

        if(count > 0 ){
            messageParent.slideDown();
            messageParent.addClass('alert');
            messageItem.html('Preencha os campos!');
            setTimeout(function(){
                messageParent.slideUp();
            }, 5000);
        }
        else if($('.vEmail').length && !IsEmail($('.vEmail', $(this)).val()))
        {
            messageParent.slideDown();
            messageParent.addClass('alert');
            messageItem.html('Preencha corretamente o E-mail');
            setTimeout(function(){
                messageParent.slideUp();
            }, 5000);
        }
        else {
            const _data = $(this).serialize();
           
            $.ajax({
                type: 'POST',
                url: 'cadastrar-usuarios',
                data: _data,
                beforeSend: function(){
                    $('button', _this).html('Enviando ...');
                    $('button', _this).attr('disabled', 'disabled');
                },
                complete : function()
                {
                    $('button', _this).html('Salvar');
                    $('button', _this).attr('disabled', false);
                },
                success: function (response) {
                    const _response = JSON.parse(response);
                    if(_response.status == true)
                    {
                        messageParent.slideDown();
                        messageItem.removeClass('success');
                        messageParent.addClass('alert');
                        messageItem.html(_response.response);
                        setTimeout(function(){
                            messageParent.slideUp();
                        }, 5000);   
                        //window.location = '/mvc/editar-'+_main.toLowerCase()+'?id='+_response.id;
                    }
                    else {
                        messageParent.slideDown();
                        messageItem.removeClass('success');
                        messageParent.addClass('alert');
                        messageItem.html(_response.response);
                        setTimeout(function(){
                            messageParent.slideUp();
                        }, 5000);
                    }
                },
                error: function (response) {
                    messageItem.addClass('alert');
                    messageItem.removeClass('loading');

                },
            });
        }
    });
}

function editarItens()
{
    $('#form_editar').submit(function(e)
    {
        e.preventDefault();
        var count = 0;
        const _this = $(this);
        const messageItem = $('.msg');
        const messageParent = messageItem.parent();
        const _url = $(this).attr('data-form');
        const _main = $(this).attr('data-main');
        const _id = $('input[name="id"]').val();

        if(messageItem.hasClass('alert'))
        {
            messageItem.removeClass('alert');
        }
        else if(messageItem.hasClass('success'))
        {
            messageItem.removeClass('success');
        }
        messageItem.html('Enviando');
        messageParent.addClass('loading');

        $( ".campoObrigatorio", _this ).each(function() {
            if($(this).val() == '')
            {
                $(this).parents('.wrapper-input').addClass('alert');
                count++;
            }
        });

        if(count > 0 ){
            messageParent.slideDown();
            messageParent.addClass('alert');
            messageItem.html('Preencha os campos!');
            setTimeout(function(){
                messageParent.slideUp();
            }, 5000);
        }
        else if($('.vEmail').length && !IsEmail($('.vEmail').val()))
        {
            messageParent.slideDown();
            messageParent.addClass('alert');
            messageItem.html('Preencha corretamente o E-mail');
            setTimeout(function(){
                messageParent.slideUp();
            }, 5000);
        }
        else {
            const _data = $(this).serialize();
            $.ajax({
                type: 'POST',
                url: _url+'?id='+_id,
                data: _data,
                beforeSend: function(){
                    $('button', _this).html('Enviando ...');
                    $('button', _this).attr('disabled', 'disabled');
                },
                complete : function()
                {
                    $('button', _this).html('Salvar');
                    $('button', _this).attr('disabled', false);
                },
                success: function (response) {
                    const _response = JSON.parse(response);
                    if(_response.status == true)
                    {
                        //window.location = '/mvc/'+_main.toLowerCase();
                        messageParent.slideDown();
                        messageItem.removeClass('alert');
                        messageParent.addClass('success');
                        messageItem.html(_response.response);
                        setTimeout(function(){
                            messageParent.slideUp();
                        }, 5000);
                    }
                    else {
                        messageParent.slideDown();
                        messageItem.removeClass('success');
                        messageParent.addClass('alert');
                        messageItem.html(_response.response);
                        setTimeout(function(){
                            messageParent.slideUp();
                        }, 5000);
                    }
                },
                error: function (response) {
                    messageItem.addClass('alert');
                    messageItem.removeClass('loading');

                },
            });
        }
    });
}

function alterarSenha()
{
    $('.box-alterar-senha a').click(function(e)
    {
        e.preventDefault();
        if(!$(this).hasClass('cancel'))
        {
            const _html = 
            '<div class="wrapper-input">\
                <input type="password" name="senha" placeholder="Senha" class="campoObrigatorio" value="">\
            </div>\
            <div class="wrapper-input">\
                <input type="password" name="confirma_senha" placeholder="Confirme a senha" class="campoObrigatorio" value="">\
            </div>';
            $(this).addClass('cancel');
            $(this).html('Cancelar');
            $(this).parent().prepend(_html);
        }
        else
        {
            $(this).removeClass('cancel');
            $(this).html('Alterar senha');
            $('.wrapper-input', $(this).parent()).remove();
        }
        
        
    })
}

function editarSenha()
{
    $('#alterar_senha').submit(function(e)
    {
        e.preventDefault();
        var count = 0;
        const _this = $(this);
        const messageItem = $('.msg');
        const messageParent = messageItem.parent();
        const _url = $(this).attr('data-form');
        const _main = $(this).attr('data-main');

        if(messageItem.hasClass('alert'))
        {
            messageItem.removeClass('alert');
        }
        else if(messageItem.hasClass('success'))
        {
            messageItem.removeClass('success');
        }
        messageItem.html('Enviando');
        messageParent.addClass('loading');

        $( ".campoObrigatorio", _this ).each(function() {
            if($(this).val() == '')
            {
                $(this).parents('.wrapper-input').addClass('alert');
                count++;
            }
        });

        if(count > 0 ){
            messageParent.slideDown();
            messageParent.addClass('alert');
            messageItem.html('Preencha os campos!');
            setTimeout(function(){
                messageParent.slideUp();
            }, 5000);
        }
        else if($('.vEmail').length && !IsEmail($('.vEmail', $(this)).val()))
        {
            messageParent.slideDown();
            messageParent.addClass('alert');
            messageItem.html('Preencha corretamente o E-mail');
            setTimeout(function(){
                messageParent.slideUp();
            }, 5000);
        }
        else {
            const _data = $(this).serialize();
           
            $.ajax({
                type: 'POST',
                url: 'editar-usuarios',
                data: _data,
                beforeSend: function(){
                    $('button', _this).html('Enviando ...');
                    $('button', _this).attr('disabled', 'disabled');
                },
                complete : function()
                {
                    $('button', _this).html('Salvar');
                    $('button', _this).attr('disabled', false);
                },
                success: function (response) {
                    const _response = JSON.parse(response);
                    if(_response.status == true)
                    {
                        messageParent.slideDown();
                        messageItem.removeClass('alert');
                        messageParent.addClass('success');
                        messageItem.html(_response.response);
                        setTimeout(function(){
                            messageParent.slideUp();
                        }, 5000);
                    }
                    else {
                        messageParent.slideDown();
                        messageItem.removeClass('success');
                        messageParent.addClass('alert');
                        messageItem.html(_response.response);
                        setTimeout(function(){
                            messageParent.slideUp();
                        }, 5000);
                    }
                },
                error: function (response) {
                    messageItem.addClass('alert');
                    messageItem.removeClass('loading');

                },
            });
        }
    });
}

function openModalExclude()
{
    $('.btn-excluir').click(function(e)
    {
        e.preventDefault();
        const _id = $(this).attr('data-id');
        $('#exclude-modal .btn-exclude').attr({'data-id': _id});

        $("#exclude-modal").modal({
            escapeClose: false,
            clickClose: false,
            showClose: false,
          });
    }) 
}

function openModalCadastro()
{
    $('.cadastrar-usuario').click(function(e)
    {
        e.preventDefault();

        $("#cadastrar-usuario-modal").modal({
            escapeClose: false,
            clickClose: true,
            showClose: true,
          });
    }) 
}

function openModalAlterarSenha()
{
    $('.forgot-password').click(function(e)
    {
        e.preventDefault();

        $("#alterar-senha-modal").modal({
            escapeClose: false,
            clickClose: true,
            showClose: true,
          });
    }) 
}

function excluirItens()
{
    $('.btn-exclude').click(function(e)
    {
        e.preventDefault();
        const _id = $(this).attr('data-id');
        const _url = $('.page-content').attr('data-page');

        $.ajax({
            type: 'POST',
            url: _url,
            data: {'id': _id, 'excluir': true},
            success: function (response) {
                const _response = JSON.parse(response);
                if(_response.status == true)
                {
                    window.location = '/mvc/'+_url;
                }
                else {
                    messageParent.slideDown();
                    messageItem.removeClass('success');
                    messageParent.addClass('alert');
                    messageItem.html(_response.response);
                    setTimeout(function(){
                        messageParent.slideUp();
                    }, 5000);
                }
            },
            error: function (response) {
                messageItem.addClass('alert');
                messageItem.removeClass('loading');

            },
        });
    })
}

function logout()
{
    $('.btn-logout').click(function(e)
    {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'login',
            data: {"logout": true},
            success: function (response) {
                const _response = JSON.parse(response);
                if(_response.status == true)
                {
                    window.location = '/mvc/home';
                }
                else {
                    
                }
            },
        });
    })
}

function IsEmail(email){
    var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
    
    if(!er.test(email))
    {
        return false;
    }
    else
    {
        return true;
    }
}

function inputFileValue()
{
    if($('input[type=file]').length)
    {
        $('input[type=file]').change(function(){
            var fileName = $(this).val().split('/').pop().split('\\').pop();
            $('span', $(this).parent()).html(fileName);
        });
    }
}

function eviarAnexo()
{
    if($('#form_anexo').length)
    {
        $('#form_anexo').submit(function(e)
        {
            e.preventDefault();
            const _this = $(this);
            const messageItem = $('.msg');
            const messageParent = messageItem.parent();
            var count = 0;
            if(messageItem.hasClass('alert'))
            {
                messageItem.removeClass('alert');
            }
            else if(messageItem.hasClass('success'))
            {
                messageItem.removeClass('success');
            }
            messageItem.html('Enviando');
            messageParent.addClass('loading');

            $( ".campoObrigatorio", _this ).each(function() {
                if($(this).val() == '')
                {
                    $(this).parents('.wrapper-input').addClass('alert');
                    count++;
                }
            });

            if(count > 0 ){
                messageParent.slideDown();
                messageParent.addClass('alert');
                messageItem.html('Preencha os campos!');
                setTimeout(function(){
                    messageParent.slideUp();
                }, 5000);
            }
            else
            {
                const _url = $('.page-content').attr('data-page');
                _data = new FormData;
                $('input', $(this)).each(function()
                {
                    if(this.getAttribute('type') === "file"){
                        _data.append('foto', $(this).get(0).files[0]);
                    }
                    else
                    {
                        _data.append(this.getAttribute("name"), this.value);
                    }
                })
            
                const _contentType = false;

                $.ajax({
                    type: 'POST',
                    url: _url,
                    processData: false,
                    contentType: _contentType,
                    data: _data,
                    beforeSend: function(){
                        $('button', _this).html('Enviando ...');
                        $('button', _this).attr('disabled', 'disabled');
                    },
                    complete : function()
                    {
                        $('button', _this).html('Salvar');
                        $('button', _this).attr('disabled', false);
                    },
                    success: function (response) {
                        const _response = JSON.parse(response);
                        if(_response.status)
                        {
                            messageParent.slideDown();
                            messageItem.removeClass('alert');
                            messageParent.addClass('success');
                            messageItem.html(_response.response);
                            setTimeout(function(){
                                messageParent.slideUp();
                            }, 5000);
                            location.reload();
                        }
                        else {
                            messageParent.slideDown();
                            messageItem.removeClass('success');
                            messageParent.addClass('alert');
                            messageItem.html(_response.response);
                            setTimeout(function(){
                                messageParent.slideUp();
                            }, 5000);
                            //modalSuccesAjax(response);
                        }
                    },
                    error: function (response) {
                        $('.box-msg .msg').html(_response.response);
                    },
                });
            }
        })
    }
}

function excluirAnexo()
{
    $('#table_anexo .btn-excluir').unbind().click(function(e)
    {
        e.preventDefault();

        const _id = $(this).attr('data-id');
        const _url = $('.page-content').attr('data-page');

        $.ajax({
            type: 'POST',
            url: _url,
            data: {'id': _id, 'excluir_anexo': true},
            success: function (response) {
                const _response = JSON.parse(response);
                if(_response.status == true)
                {
                    //window.location = '/mvc/'+_url;
                    location.reload();
                }
                else {
                    messageParent.slideDown();
                    messageItem.removeClass('success');
                    messageParent.addClass('alert');
                    messageItem.html(_response.response);
                    setTimeout(function(){
                        messageParent.slideUp();
                    }, 5000);
                }
            },
            error: function (response) {
                messageItem.addClass('alert');
                messageItem.removeClass('loading');

            },
        });
    })
}

function activeSlider()
{
    $('.banner-box.owl-carousel').owlCarousel({
        loop:true,
        margin:0,
        responsiveClass:true,
        nav: true,
        navText : ["<i class='spt-arrow-prev'></i>","<i class='spt-arrow-next'></i>"],  
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:true
            },
            1000:{
                items:1,
                nav:true,
                loop:true
            }
        }
    })
}

function boxEstadosSlider()
{
    $('#owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        responsiveClass:true,
        nav: true,
        navText : ["<i class='spt-arrow-prev'></i>","<i class='spt-arrow-next'></i>"],  
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:true
            },
            1000:{
                items:8,
                nav:true,
            }
        }
    })
}

function EstadoFiltro()
{
    $('.box-estados .estados-item').click(function(e)
    {
        e.preventDefault();

        if(!$(this).hasClass('active'))
        {
            $('.box-estados .estados-item').removeClass('active');
            $(this).addClass('active')
        }
        const _uf = $(this).attr('data-uf');
        
        $.ajax({
            type: 'POST',
            url: 'home',
            data: {'id_estado': _uf},
            success: function (response) {
                const _response = JSON.parse(response);
                if(_response.status == true)
                {
                    console.log(_response.response);
                    $('.box-viagens').html(_response.response);
                    //window.location = '/mvc/'+_url;
                    //  location.reload();
                }
                else {
                    messageParent.slideDown();
                    messageItem.removeClass('success');
                    messageParent.addClass('alert');
                    messageItem.html(_response.response);
                    setTimeout(function(){
                        messageParent.slideUp();
                    }, 5000);
                }
            },
            error: function (response) {
                messageItem.addClass('alert');
                messageItem.removeClass('loading');

            },
        });
    })
}

function pesquisaPublicacaoByEstado()
{
    $('.inputPesquisa').change(function()
    {
        var _value = $(this).val();

        $.ajax({
            type: 'POST',
            url: 'home',
            data: {'estado': _value},
            success: function (response) {
                const _response = JSON.parse(response);
                if(_response.status == true)
                {
                    console.log(_response.response);
                    $('.box-viagens').html(_response.response);
                    //window.location = '/mvc/'+_url;
                    //  location.reload();
                }
                else {
                    messageParent.slideDown();
                    messageItem.removeClass('success');
                    messageParent.addClass('alert');
                    messageItem.html(_response.response);
                    setTimeout(function(){
                        messageParent.slideUp();
                    }, 5000);
                }
            },
            error: function (response) {
                messageItem.addClass('alert');
                messageItem.removeClass('loading');

            },
        });
    })
}

