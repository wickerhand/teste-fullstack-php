<?php
namespace config;

class DataBase {
    private $conection;

    public function __construct()
    {
        $this->conection = new \PDO("mysql:dbname=db_phidelis;host=localhost", "root", "");
    }

    public function request($string)
    {
        $sql = $this->conection->prepare($string);

        return $sql;
    }

    public static function pdoException($code)
    {
        Switch($code)
        {
            case 2002:
                return "Problemas ao conectar com o banco de dados, verifique sua conexão com a internet.";
                break;
            case 1049:
                //senha errada
                return "Verifique os parametros de conexão com o banco de dados codigo: ".$code;
                break;
            default:
                return "Erro na conexão com o banco de dados, código: ".$code;
                break;
        }
    }
}