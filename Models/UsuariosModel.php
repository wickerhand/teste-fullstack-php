<?php

namespace Models;

use config\DataBase;

class UsuariosModel {

    public static function validarLogin($email)
    {
        $conection = new DataBase();
        
        $sql = $conection->request("SELECT * FROM usuarios WHERE email = ?");
        $sql->execute(array($email));

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function fetchAll()
    {
        try
        {
            $conection = new DataBase();

            $sql = $conection->request("SELECT * FROM usuarios");
            $sql->execute();
            $response = $sql->fetchAll();
    
            return $response;
        }
        catch(\PDOException $e)
        {
            echo 'Problemas com a conexão, contate o nosso suporte!';
        }
        
    }

    public static function getUsuarioById($id)
    {
        $conection = new DataBase();

        $sql = $conection->request("SELECT * FROM usuarios WHERE id_usuario = ?");
        $sql->execute(array($id));
        $response = $sql->fetchAll();

        return $response;
    }

    public static function cadastrar($usuario, $email, $senha)
    {
        $conection = new DataBase();
        
        $sql = $conection->request("INSERT INTO usuarios (usuario, email, senha, status) VALUES (?, ?, ?, 'A')");
        $sql->execute(array($usuario, $email, $senha));

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function editar($id, $usuario, $email, $senha = '', $status)
    {
        $conection = new DataBase();
        $arrayData = array();

        if(!empty($senha))
        {
            $query = 'UPDATE usuarios SET usuario = ?, email = ?, senha = ?, status = ? WHERE id_usuario = ?';
            $arrayData = array($usuario, $email, $senha, $status, $id);
        }
        else
        {
            $query = 'UPDATE usuarios SET usuario = ?, email = ?, status = ? WHERE id_usuario = ?';
            $arrayData = array($usuario, $email, $status, $id);
        }
        
        $sql = $conection->request($query);
        $sql->execute($arrayData);

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function excluir($id)
    {
        $conection = new DataBase();
        
        $sql = $conection->request("DELETE FROM usuarios WHERE id_usuario = ?");
        $sql->execute(array($id));

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function getUltimoRegistro()
    {
        $conection = new DataBase();
        
        $sql = $conection->request("SELECT id_usuario FROM usuarios ORDER BY id_usuario DESC LIMIT 1");
        $sql->execute();

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function AlterarSenha($senha, $id)
    {
        $conection = new DataBase();
        
        $sql = $conection->request('UPDATE usuarios SET senha = ? WHERE id_usuario = ?');
        $sql->execute(array($senha, $id));
    }
}
?>