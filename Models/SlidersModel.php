<?php

namespace Models;

use config\DataBase;

class SlidersModel {

    public static function fetchAll()
    {
        try
        {
            $conection = new DataBase();

            $sql = $conection->request("SELECT * FROM sliders");
            $sql->execute();
            $response = $sql->fetchAll();
    
            return $response;
        }
        catch(\PDOException $e)
        {
            echo 'Problemas com a conexão, contate o nosso suporte!';
        }
        
    }

    public static function getSliderById($id)
    {
        $conection = new DataBase();

        $sql = $conection->request("SELECT * FROM sliders WHERE id_slider = ?");
        $sql->execute(array($id));
        $response = $sql->fetchAll();

        return $response;
    }

    public static function cadastrar($titulo_slider)
    {
        $conection = new DataBase();
        
        $sql = $conection->request("INSERT INTO sliders (titulo_slider) VALUES (?)");
        $sql->execute(array($titulo_slider));
    }

    public static function editar($id, $slider)
    {
        $conection = new DataBase();
        
        $sql = $conection->request('UPDATE sliders SET titulo_slider = ? WHERE id_slider = ?');
        $sql->execute(array($slider, $id));

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function excluir($id)
    {
        $conection = new DataBase();
        
        $sql = $conection->request("DELETE FROM sliders WHERE id_slider = ?");
        $sql->execute(array($id));

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function getUltimoRegistro()
    {
        $conection = new DataBase();
        
        $sql = $conection->request("SELECT id_slider FROM sliders ORDER BY id_slider DESC LIMIT 1");
        $sql->execute();

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function cadastrarImagem($nome_imagem, $texto_banner, $id)
    {
        $conection = new DataBase();

        $sql = $conection->request("INSERT INTO imagens_slider (nome_imagem, texto_banner, id_slider) VALUES (?, ?, ?)");
        $sql->execute(array($nome_imagem, $texto_banner, $id));   
    }

    public static function getImages($id)
    {
        $conection = new DataBase();
        
        $sql = $conection->request("SELECT * FROM imagens_slider im INNER JOIN sliders sl ON im.id_slider = sl.id_slider WHERE im.id_slider = ?");
        $sql->execute(array($id));

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function getSliderByName($name)
    {
        $conection = new DataBase();
        
        $sql = $conection->request("SELECT * FROM imagens_slider im INNER JOIN sliders sl ON im.id_slider = sl.id_slider WHERE sl.titulo_slider = ?");
        $sql->execute(array($name));

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function deleteAnexo($id)
    {
        $conection = new DataBase();
        
        $sql = $conection->request("DELETE FROM imagens_slider WHERE id_imagem_slider = ?");
        $sql->execute(array($id));

    }
}
?>