<?php

namespace Models;

use config\DataBase;

class EstadosModel {

    public static function fetchAll()
    {
        try
        {
            $conection = new DataBase();

            $sql = $conection->request("SELECT * FROM estados");
            $sql->execute();
            $response = $sql->fetchAll();
    
            return $response;
        }
        catch(\PDOException $e)
        {
            echo 'Problemas com a conexão, contate o nosso suporte!';
        }
        
    }

    public static function getEstadoById($id)
    {
        $conection = new DataBase();

        $sql = $conection->request("SELECT * FROM estados WHERE id_estado = ?");
        $sql->execute(array($id));
        $response = $sql->fetchAll();

        return $response;
    }

    public static function cadastrar($estado, $uf)
    {
        $conection = new DataBase();
        
        $sql = $conection->request("INSERT INTO estados (estado, uf) VALUES (?, ?)");
        $sql->execute(array($estado, $uf));

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function editar($id, $estado, $uf)
    {
        $conection = new DataBase();
        
        $sql = $conection->request("UPDATE estados SET estado = ?, uf = ? WHERE id_estado = ?");
        $sql->execute(array($estado, $uf, $id));

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function excluir($id)
    {
        $conection = new DataBase();
        
        $sql = $conection->request("DELETE FROM estados WHERE id_estado = ?");
        $sql->execute(array($id));

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function getUltimoRegistro()
    {
        $conection = new DataBase();
        
        $sql = $conection->request("SELECT id_estado FROM estado ORDER BY id_estado DESC LIMIT 1");
        $sql->execute();

        $response = $sql->fetchAll();
        
        return $response;
    }
}
?>