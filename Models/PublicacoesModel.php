<?php

namespace Models;

use config\DataBase;

class PublicacoesModel {

    public static function fetchAll()
    {
        try
        {
            $conection = new DataBase();

            $sql = $conection->request("SELECT * FROM publicacoes p
            INNER JOIN usuarios u ON u.id_usuario = p.id_usuario");
            $sql->execute();
            $response = $sql->fetchAll();
    
            return $response;
        }
        catch(\PDOException $e)
        {
            echo 'Problemas com a conexão, contate o nosso suporte!';
        }
        
    }

    public static function getPublicacaoById($id)
    {
        $conection = new DataBase();

        $sql = $conection->request("SELECT p.*, u.usuario FROM publicacoes p INNER JOIN usuarios u  ON p.id_usuario = u.id_usuario WHERE id_publicacao = ?");
        $sql->execute(array($id));
        $response = $sql->fetchAll();

        return $response;
    }

    public static function cadastrar($id_usuario, $titulo, $id_estado, $iframe, $descricao)
    {
        $conection = new DataBase();
        
        $sql = $conection->request("INSERT INTO publicacoes (titulo, id_estado, id_usuario, iframe_video, descricao) VALUES (?, ?, ?, ?, ?)");
        $sql->execute(array($titulo, $id_estado, $id_usuario, $iframe, $descricao,));

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function editar($id, $titulo, $id_estado, $iframe, $descricao)
    {
        $conection = new DataBase();
        
        $sql = $conection->request("UPDATE publicacoes SET titulo = ?, id_estado = ?, iframe_video = ?, descricao = ? WHERE id_publicacao = ?");
        $sql->execute(array($titulo, $id_estado, $iframe, $descricao, $id));

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function excluir($id)
    {
        $conection = new DataBase();
        
        $sql = $conection->request("DELETE FROM publicacoes WHERE id_publicacao = ?");
        $sql->execute(array($id));

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function getUltimoRegistro()
    {
        $conection = new DataBase();
        
        $sql = $conection->request("SELECT id_publicacao FROM publicacoes ORDER BY id_publicacao DESC LIMIT 1");
        $sql->execute();

        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function getPublicacoesByUF($id_estado)
    {
        $conection = new DataBase();
        $where = '';
        if($id_estado != '0')
        {
            $where = 'WHERE id_estado = ?';
        }


    

        $sql = $conection->request("SELECT * FROM publicacoes ".$where);
        if($id_estado != '0')
        {
            $sql->execute(array($id_estado));
        }
        else
        {
            $sql->execute();
        }
        
        $response = $sql->fetchAll();
        
        return $response;
    }

    public static function getPublicacoesByEstado($estado)
    {
        $conection = new DataBase();
        $where = '';
        $id_estado = '';
        if(!empty($estado))
        {
            $where = "WHERE estado LIKE '".$estado."%'";
        }
       
        $sql = $conection->request("SELECT id_estado FROM estados ".$where.' ORDER BY id_estado ASC LIMIT 1');
        if(!empty($estado))
        {
            $sql->execute(array($estado));
        }
        else
        {
            $sql->execute();
        }
        
        $response = $sql->fetchAll();

        if(!empty($response))
        {
            $id_estado = $response[0]['id_estado'];
        }
        
        $publicacoes = PublicacoesModel::getPublicacoesByUF($id_estado);

        return $publicacoes;
    }
}
?>