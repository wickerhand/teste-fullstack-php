<?php
	
	namespace Controllers;
	class DashboardController
	{
		public function __construct(){
			$this->view = new \Views\MainView('dashboard');
		}
		public function executar(){
			session_start();
			
			if(!isset($_SESSION['logado']))
			{
				header('Location: login');
				exit();
			}
			$this->view->render(array('titulo'=>'Dashboard'));
		}
	}
?>