<?php
	
	namespace Controllers;

	use Models\PublicacoesModel;
	use config\DataBase;
	class PublicacoesController
	{
		public function __construct(){
			$this->view = new \Views\MainView('publicacoes');
		}
		public function executar(){
			session_start();
			
			if(!isset($_SESSION['logado']))
			{
				header('Location: login');
				exit();
			}

			if(isset($_POST['excluir']))
			{
				$this->excluir();
			}

			$arrayView = array(
				'titulo'=>'Publicacoes',
				'publicacoes' => PublicacoesModel::fetchAll(),
				'page_config' => 'admin'
			);

			
			$this->view->render($arrayView);
		}

		public function excluir()
		{
			$arrayJson = array("status" => false);
			try
			{	
				PublicacoesModel::excluir($_POST['id']);
				$arrayJson['status'] = true;
				$arrayJson['response'] = "Publicacao excluído com sucesso!";
			}
			catch(\PDOException $e)
			{
				$arrayJson['response'] = DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}

			echo json_encode($arrayJson);
			die;
		}
	}
?>