<?php
	
	namespace Controllers;

	use Models\EstadosModel;
	use config\DataBase;
	class EstadosController
	{
		public function __construct(){
			$this->view = new \Views\MainView('estados');
		}
		public function executar(){
			session_start();
			
			if(!isset($_SESSION['logado']))
			{
				header('Location: login');
				exit();
			}

			if(isset($_POST['excluir']))
			{
				$this->excluir();
			}

			$arrayView = array(
				'titulo'=>'Estados',
				'estados' => EstadosModel::fetchAll(),
				'page_config' => 'admin'
			);
			
			$this->view->render($arrayView);
		}

		public function excluir()
		{
			$arrayJson = array("status" => false);
			try
			{	
				EstadosModel::excluir($_POST['id']);
				$arrayJson['status'] = true;
				$arrayJson['response'] = "Estado excluído com sucesso!";
			}
			catch(\PDOException $e)
			{
				$arrayJson['response'] = DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}

			echo json_encode($arrayJson);
			die;
		}
	}
?>