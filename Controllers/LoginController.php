<?php
	
	namespace Controllers;

	use Models\UsuariosModel;
	use \Exception;
	use config\DataBase;

	class LoginController
	{

		public function __construct(){
			$this->view = new \Views\MainView('login');
		}
		public function executar(){
			session_start();

			if(!empty($_POST['logout']))
			{
				$this->logout();
			}

			if(isset($_SESSION['logado']))
			{
				header('Location: dashboard');
				exit();
			}

			if(!empty($_POST['email']))
			{
				$this->loginUser();
			}
			
			$this->view->render(array('titulo'=>'Login'));
		}

		public function loginUser()
		{
			$arrayJson = array("status" => false);

			try{
				$user = UsuariosModel::validarLogin($_POST['email']);
				
				if(empty($user) || !password_verify($_POST['password'], $user[0]['senha']))
				{
					throw new \Exception('E-mail ou senha incorretos');
				}

				if($user[0]['status'] == 'I' || $user[0]['senha'] == "B")
				{
					throw new \Exception("Essa conta encontra-se inativa ou bloqueada, para mais informações contate o administrador do site");
				}
				
				$_SESSION['logado'] = array();
				$_SESSION['logado']['id'] = $user[0]['id_usuario'];
				$_SESSION['logado']['nome_usuario'] = $user[0]['usuario'];
				$_SESSION['logado']['email_usuario'] = $user[0]['email'];

				$arrayJson['status'] = true;
			}
			catch(\PDOException $e)
			{
				DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}
			
			echo json_encode($arrayJson);
			die;
		}

		public function logout()
		{
			$arrayJson = array("status" => false);
			
			session_destroy();

			$arrayJson["status"] = true;

			echo json_encode($arrayJson);
			die;
		}

		public static function teste()
		{
			echo 'aaaaaaaa';
		}
	}
?>