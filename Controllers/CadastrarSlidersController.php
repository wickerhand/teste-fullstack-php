<?php
	
	namespace Controllers;

	use Models\SlidersModel;
	use config\DataBase;

	class CadastrarSlidersController
	{
		public function __construct(){
			$this->view = new \Views\MainView('dashboard-forms');
		}
		public function executar(){
			session_start();
			
			if(!isset($_SESSION['logado']))
			{
				header('Location: login');
				exit();
			}

			if(!empty($_POST['cadastrar']))
			{
				$this->cadastrar();
			}

			$arrayView = array(
				'titulo'=>'Sliders',
				'form_type' => 'slider',
				'titulo_form' => 'Cadastrar sliders',
				'form_id' => 'cadastrar',
				'data_form' => 'cadastrar-sliders',
				'page_config' => 'admin'
			);

			$this->view->render($arrayView);
		}

		public function cadastrar()
		{
			$arrayJson = array("status" => false);
			try
			{	
				SlidersModel::cadastrar($_POST['titulo_slider']);
				$ultimoRegistro = SlidersModel::getUltimoRegistro();
				$arrayJson['status'] = true;
				$arrayJson['response'] = "Slider cadastrado com sucesso!";
				$arrayJson['id'] = $ultimoRegistro[0]['id_slider']; 
			}
			catch(\PDOException $e)
			{
				
				$arrayJson['response'] = DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}

			echo json_encode($arrayJson);
			die;
		}
	}
?>