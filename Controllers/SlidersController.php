<?php
	
namespace Controllers;

use config\DataBase;
use Models\SlidersModel;

class SlidersController
	{
		public function __construct(){
			$this->view = new \Views\MainView('sliders');
		}
		public function executar(){
			session_start();
			
			if(!isset($_SESSION['logado']))
			{
				header('Location: login');
				exit();
			}

			if(isset($_POST['excluir']))
			{
				$this->excluir();
			}

			$arrayView = array(
				'titulo'=>'Sliders',
				'sliders' => SlidersModel::fetchAll(),
				'page_config' => 'admin'
			);
			
			$this->view->render($arrayView);
		}

		public function excluir()
		{
			$arrayJson = array("status" => false);
			try
			{	
				SlidersModel::excluir($_POST['id']);
				$arrayJson['status'] = true;
				$arrayJson['response'] = "Slider excluído com sucesso!";
			}
			catch(\PDOException $e)
			{
				$e->getMessage();
				$arrayJson['response'] = DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}

			echo json_encode($arrayJson);
			die;
		}
	}
?>