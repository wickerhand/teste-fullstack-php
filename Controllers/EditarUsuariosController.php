<?php
	
	namespace Controllers;

	use Models\UsuariosModel;
	use config\DataBase;

	class EditarUsuariosController
	{
		public function __construct(){
			$this->view = new \Views\MainView('dashboard-forms');
		}
		public function executar(){
			session_start();
			if(!empty($_POST['alterar_senha']))
			{
				$this->editarSenha();
			}

			if(!isset($_SESSION['logado']))
			{
				header('Location: login');
				exit();
			}
			if(!isset($_GET['id']))
			{
				header('Location: usuarios');
				exit();
			}

			if(!empty($_POST['editar']))
			{
				$this->editar();
			}

			$arrayView = array(
				'titulo'=>'Usuarios',
				'form_type' => 'usuario',
				'titulo_form' => 'Editar usuarios',
				'usuario' => UsuariosModel::getUsuarioById($_GET['id']),
				'form_id' => 'editar',
				'data_form' => 'editar-usuarios',
				'page_config' => 'admin'
			);

			$this->view->render($arrayView);
		}

		public function editar()
		{
			$arrayJson = array("status" => false);
			try
			{	
				$senha ='';
				if(!empty($_POST['senha']) && $_POST['senha'] != $_POST['confirma_senha'])
				{
					throw new \Exception("As senhas devem ser iguais");
				}
				if(!empty($_POST['senha']))
				{
					$senha = password_hash($_POST['senha'], PASSWORD_BCRYPT);
				}
				
				UsuariosModel::editar($_POST['id'], $_POST['usuario'], $_POST['email'], $senha, $_POST['status']);
				$arrayJson['status'] = true;
				$arrayJson['response'] = "Usuario editado com sucesso!";
			}
			catch(\PDOException $e)
			{
				$arrayJson['response'] = DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}

			echo json_encode($arrayJson);
			die;
		}

		public function editarSenha()
		{
			$arrayJson = array("status" => false);

			try{
				$user = UsuariosModel::validarLogin($_POST['email']);
				
				if(empty($user))
				{
					throw new \Exception("Não foi possível encontrar esse usuário");
				}

				if($_POST['senha'] != $_POST['confirma_senha'])
				{
					throw new \Exception("Senha e confirmar senha devem ser iguais");
				}

				$senha = password_hash($_POST['senha'], PASSWORD_BCRYPT);
				
				UsuariosModel::AlterarSenha($senha, $user[0]['id_usuario']);

				$arrayJson['status'] = true;
				$arrayJson['response'] = "Senha alterada com sucesso!";
			}
			catch(\PDOException $e)
			{
				DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}
			
			echo json_encode($arrayJson);
			die;
		}
	}
?>