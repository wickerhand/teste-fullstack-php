<?php
	
	namespace Controllers;

	use Models\EstadosModel;
	use config\DataBase;

	class CadastrarEstadosController
	{
		public function __construct(){
			$this->view = new \Views\MainView('dashboard-forms');
		}
		public function executar(){
			session_start();
			
			if(!isset($_SESSION['logado']))
			{
				header('Location: login');
				exit();
			}

			if(!empty($_POST['cadastrar']))
			{
				$this->cadastrar();
			}

			$arrayView = array(
				'titulo'=>'Estados',
				'form_type' => 'estado',
				'titulo_form' => 'Cadastrar estados',
				'form_id' => 'cadastrar',
				'data_form' => 'cadastrar-estados'
			);

			$this->view->render($arrayView);
		}

		public function cadastrar()
		{
			$arrayJson = array("status" => false);
			try
			{	
				EstadosModel::cadastrar($_POST['estado'], $_POST['uf']);
				$ultimoRegistro = EstadosModel::getUltimoRegistro();
				$arrayJson['status'] = true;
				$arrayJson['response'] = "Estado cadastrado com sucesso!";
				$arrayJson['id'] = $ultimoRegistro[0]['id_estado']; 
			}
			catch(\PDOException $e)
			{
				
				$arrayJson['response'] = DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}

			echo json_encode($arrayJson);
			die;
		}
	}
?>