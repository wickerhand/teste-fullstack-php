<?php
	
namespace Controllers;

use config\DataBase;
use Models\UsuariosModel;

class UsuariosController
	{
		public function __construct(){
			$this->view = new \Views\MainView('usuarios');
		}
		public function executar(){
			session_start();
			
			if(!isset($_SESSION['logado']))
			{
				header('Location: login');
				exit();
			}

			if(isset($_POST['excluir']))
			{
				$this->excluir();
			}

			$arrayView = array(
				'titulo'=>'Usuarios',
				'usuarios' => UsuariosModel::fetchAll(),
				'page_config' => 'admin'
			);
			
			$this->view->render($arrayView);
		}

		public function excluir()
		{
			$arrayJson = array("status" => false);
			try
			{	
				UsuariosModel::excluir($_POST['id']);
				$arrayJson['status'] = true;
				$arrayJson['response'] = "Estado excluído com sucesso!";
			}
			catch(\PDOException $e)
			{
				$e->getMessage();
				$arrayJson['response'] = DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}

			echo json_encode($arrayJson);
			die;
		}
	}
?>