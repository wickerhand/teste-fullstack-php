<?php
	
	namespace Controllers;

	use Models\PublicacoesModel;
	use config\DataBase;
use Exception;
use Models\EstadosModel;

class EditarPublicacoesController
	{
		public function __construct(){
			$this->view = new \Views\MainView('dashboard-forms');
		}
		public function executar(){
			session_start();
			
			if(!isset($_SESSION['logado']))
			{
				header('Location: login');
				exit();
			}
			if(!isset($_GET['id']))
			{
				header('Location: publicacoes');
				exit();
			}

			if(!empty($_POST['editar']))
			{
				$this->editar();
			}

			$arrayView = array(
				'titulo'=>'Publicacoes',
				'form_type' => 'publicacao',
				'titulo_form' => 'Editar publicacoes',
				'publicacao' => PublicacoesModel::getPublicacaoById($_GET['id']),
				'estados' => EstadosModel::fetchAll(),
				'form_id' => 'editar',
				'data_form' => 'editar-publicacoes',
				'page_config' => 'admin'
			);

			$this->view->render($arrayView);
		}

		public function editar()
		{
			$arrayJson = array("status" => false);
			try
			{	
				PublicacoesModel::editar($_POST['id'], $_POST['titulo'], $_POST['id_estado'], $_POST['iframe'], $_POST['descricao']);

				$arrayJson['status'] = true;
				$arrayJson['response'] = "Publicacao editada com sucesso!";
			}
			catch(\PDOException $e)
			{
				$arrayJson['response'] = DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}
			catch(Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}

			echo json_encode($arrayJson);
			die;
		}
	}
?>