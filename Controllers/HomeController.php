<?php
	
	namespace Controllers;

use config\DataBase;
use Models\EstadosModel;
use Models\PublicacoesModel;
use Models\SlidersModel;

class HomeController
{

	public function __construct(){
		$this->view = new \Views\MainView('home');
	}
	public function executar(){

		if(isset($_POST['id_estado']))
		{
			$this->selecPublicacoesByUF();
		}

		if(isset($_POST['estado']))
		{
			$this->selecPublicacoesByEstado();
		}

		$arrayView = array(
			"titulo" => "Home",
			"page_config" => '',
			"estados" => EstadosModel::fetchAll(),
			"publicacoes" =>PublicacoesModel::getPublicacoesByUF(0),
			"sliders" => SlidersModel::getSliderByName('Home')
		);

		$this->view->render($arrayView);
	}

	public function selecPublicacoesByUF()
	{
		$arrayJson = array("status" => false);

		try
		{
			$publicacoes = PublicacoesModel::getPublicacoesByUF($_POST['id_estado']);
			$html = '';

			foreach($publicacoes as $publicacao)
			{
				$html .= '
				<div class="box-viagens-item">
					<a href="publicacao?id='.$publicacao['id_publicacao'].'" class="pub-link">
						<h4>'.$publicacao['titulo'].'</h4>
						<div class="border-item"></div>
					</a>
				</div>';
			}

			$arrayJson['response'] = $html;
			$arrayJson['status'] = true;
		}
		catch(\PDOException $e)
		{
			$arrayJson['response'] = DataBase::pdoException($e->getCode());
		}
		catch(\Exception $e)
		{
			$arrayJson['response'] = $e->getMessage();
		}

		
		echo json_encode($arrayJson);
		die;
	}

	public function selecPublicacoesByEstado()
	{
		$arrayJson = array("status" => false);

		try
		{
			$publicacoes = PublicacoesModel::getPublicacoesByEstado($_POST['estado']);
			$html = '';
			if(!empty($publicacoes))
			{
				foreach($publicacoes as $publicacao)
				{
					$html .= '
					<div class="box-viagens-item">
						<a href="publicacao?id='.$publicacao['id_publicacao'].'" class="pub-link">
							<h4>'.$publicacao['titulo'].'</h4>
							<div class="border-item"></div>
						</a>
					</div>';
				}
			}
			else
			{
				$html = '<p>Desculpe, não encontramos pubicações para esses estado</p>';
			}
			

			$arrayJson['response'] = $html;
			$arrayJson['status'] = true;
		}
		catch(\PDOException $e)
		{
			$arrayJson['response'] = DataBase::pdoException($e->getCode());
		}
		catch(\Exception $e)
		{
			$arrayJson['response'] = $e->getMessage();
		}

		
		echo json_encode($arrayJson);
		die;
	}
}
?>