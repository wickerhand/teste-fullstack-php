<?php
	
	namespace Controllers;

	use Models\PublicacoesModel;
	use config\DataBase;
use Models\EstadosModel;

class CadastrarPublicacoesController
	{
		public function __construct(){
			$this->view = new \Views\MainView('dashboard-forms');
		}
		public function executar(){
			session_start();
			
			if(!isset($_SESSION['logado']))
			{
				header('Location: login');
				exit();
			}

			if(!empty($_POST['cadastrar']))
			{
				$this->cadastrar();
			}

			$arrayView = array(
				'titulo'=>'Publicacoes',
				'form_type' => 'publicacao',
				'titulo_form' => 'Cadastrar publicacoes',
				'form_id' => 'cadastrar',
				'data_form' => 'cadastrar-publicacoes',
				'estados' => EstadosModel::fetchAll(),
				'page_config' => 'admin'
			);

			$this->view->render($arrayView);
		}

		public function cadastrar()
		{
			$arrayJson = array("status" => false);
			try
			{	
				PublicacoesModel::cadastrar($_SESSION['logado']['id'], $_POST['titulo'], $_POST['id_estado'], $_POST['iframe'], $_POST['descricao']);
				$ultimoRegistro = PublicacoesModel::getUltimoRegistro();
				$arrayJson['status'] = true;
				$arrayJson['response'] = "Publicacao cadastrado com sucesso!";
				$arrayJson['id'] = $ultimoRegistro[0]['id_publicacao']; 
			}
			catch(\PDOException $e)
			{
				
				$arrayJson['response'] = DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}

			echo json_encode($arrayJson);
			die;
		}
	}
?>