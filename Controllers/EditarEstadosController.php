<?php
	
	namespace Controllers;

	use Models\EstadosModel;
	use config\DataBase;

	class EditarEstadosController
	{
		public function __construct(){
			$this->view = new \Views\MainView('dashboard-forms');
		}
		public function executar(){
			session_start();
			if(!isset($_SESSION['logado']))
			{
				header('Location: login');
				exit();
			}
			if(!isset($_GET['id']))
			{
				header('Location: estados');
				exit();
			}

			if(!empty($_POST['editar']))
			{
				$this->editar();
			}

			$arrayView = array(
				'titulo'=>'Estados',
				'form_type' => 'estado',
				'titulo_form' => 'Editar estados',
				'estado' => EstadosModel::getEstadoById($_GET['id']),
				'form_id' => 'editar',
				'data_form' => 'editar-estados',
				'page_config' => 'admin'
			);

			$this->view->render($arrayView);
		}

		public function editar()
		{
			$arrayJson = array("status" => false);
			try
			{	
				EstadosModel::editar($_POST['id'], $_POST['estado'], $_POST['uf']);
				$arrayJson['status'] = true;
				$arrayJson['response'] = "Estado editado com sucesso!";
			}
			catch(\PDOException $e)
			{
				$arrayJson['response'] = DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}

			echo json_encode($arrayJson);
			die;
		}
	}
?>