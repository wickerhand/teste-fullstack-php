<?php
	
	namespace Controllers;

	use Models\SlidersModel;
	use config\DataBase;

	class EditarSlidersController
	{
		public function __construct(){
			$this->view = new \Views\MainView('dashboard-forms');
		}
		public function executar(){
			session_start();
			if(!isset($_SESSION['logado']))
			{
				header('Location: login');
				exit();
			}
			if(!isset($_GET['id']))
			{
				header('Location: sliders');
				exit();
			}

			if(!empty($_POST['editar']))
			{
				$this->editar();
			}

			if(isset($_POST['excluir_anexo']))
			{
				$this->excluirAnexo();
			}

			if(isset($_FILES['foto']))
			{
				$this->imageUpload();
			}

			$arrayView = array(
				'titulo'=>'Sliders',
				'form_type' => 'slider',
				'titulo_form' => 'Editar sliders',
				'slider' => SlidersModel::getSliderById($_GET['id']),
				'imagens_slider' => SlidersModel::getImages($_GET['id']),
				'form_id' => 'editar',
				'data_form' => 'editar-sliders',
				'page_config' => 'admin'
			);

			$this->view->render($arrayView);
		}

		public function editar()
		{
			$arrayJson = array("status" => false);
			try
			{	
				SlidersModel::editar($_POST['id'], $_POST['titulo_slider']);
				$arrayJson['status'] = true;
				$arrayJson['response'] = "Slider editado com sucesso!";
			}
			catch(\PDOException $e)
			{
				$arrayJson['response'] = DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}

			echo json_encode($arrayJson);
			die;
		}

		public function imageUpload()
		{
			$arrayJson = array("status" => false);
			try{
				//Define the path for upload
				if($_SERVER['SERVER_NAME'] == 'localhost' || $_SERVER['SERVER_NAME'] == '127.0.0.1')
				{
					$uploadDir = 'C:/xampp/htdocs/mvc/public/uploads/';
				}
				else
				{
					$uploadDir = public_path.'uploads/';
				}

				//Change all characters to lowercase
				$nameToLower = strtolower($_FILES['foto']['name']);

				//Change all spaces for traces
				$nameToLower = str_replace(" ", "-", $nameToLower);

				//Define the allowed extensions
				$allowedExts = array("jpeg", "jpg", "png");

				//Catch the file extension
				$extension = explode("/", $_FILES['foto']['type']);
				$extension = $extension[1];
				$isAllowed = false;
				$expectedWidth = 1920;
				$expectedHeight = 770;
				list($width, $height) = getimagesize($_FILES['foto']['tmp_name']);
				$uploadfile = $uploadDir . $nameToLower;

				//Verify if the file extension is allowed to upload
				for($i=0; $i < count($allowedExts); $i++)
				{
					if($allowedExts[$i] == $extension)
					{
						$isAllowed = true;
					}
				}

				if(!$isAllowed)
				{
					throw new \Exception('As imagens devem estar em um desses formatos: JPG, JPEG ou PNG');
				}

				if($width != $expectedWidth || $height != $expectedHeight)
				{
					throw new \Exception("A imagem deve ter as dimenções de Largura: ".$expectedWidth."px, Altura: ".$expectedHeight."px");
				}

				move_uploaded_file($_FILES['foto']['tmp_name'], $uploadfile);
				SlidersModel::cadastrarImagem($nameToLower, $_POST['texto_banner'], $_POST['id']);
				$arrayJson['response'] = "Imagem cadastrada com sucesso!";
				$arrayJson['status'] = true;

			}
			catch(\PDOException $e)
			{
				$arrayJson['response'] = DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}

			echo json_encode($arrayJson);
			die;
		}

		public function excluirAnexo()
		{
			$arrayJson = array("status" => false);
			try
			{
				SlidersModel::deleteAnexo($_POST['id']);
				$arrayJson['response'] = "Imagem excluida com sucesso!";
				$arrayJson['status'] = true;
			}
			catch(\PDOException $e)
			{
				$arrayJson['response'] = DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}
			echo json_encode($arrayJson);
			die;
		}
	}
?>