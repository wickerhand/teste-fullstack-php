<?php
	
	namespace Controllers;

	use Models\UsuariosModel;
	use config\DataBase;

	class CadastrarUsuariosController
	{
		public function __construct(){
			$this->view = new \Views\MainView('dashboard-forms');
		}
		public function executar(){
			session_start();

			if(!empty($_POST['cadastrar']))
			{
				$this->cadastrar();
			}
			
			if(!isset($_SESSION['logado']))
			{
				header('Location: login');
				exit();
			}

			$arrayView = array(
				'titulo'=>'Usuarios',
				'form_type' => 'usuario',
				'titulo_form' => 'Cadastrar usuarios',
				'form_id' => 'cadastrar',
				'data_form' => 'cadastrar-usuarios'
			);

			$this->view->render($arrayView);
		}

		public function cadastrar()
		{
			$arrayJson = array("status" => false);
			try
			{	
				if($_POST['senha'] != $_POST['confirma_senha'])
				{
					throw new \Exception("As senhas devem ser iguais");
				}
				$senha = password_hash($_POST['senha'], PASSWORD_BCRYPT);
				UsuariosModel::cadastrar($_POST['usuario'], $_POST['email'], $senha);
				$ultimoRegistro = UsuariosModel::getUltimoRegistro();
				$arrayJson['status'] = true;
				$arrayJson['response'] = "Usuario cadastrado com sucesso!";
				$arrayJson['id'] = $ultimoRegistro[0]['id_usuario'];
			}
			catch(\PDOException $e)
			{
				
				$arrayJson['response'] = DataBase::pdoException($e->getCode());
			}
			catch(\Exception $e)
			{
				$arrayJson['response'] = $e->getMessage();
			}

			echo json_encode($arrayJson);
			die;
		}
	}
?>