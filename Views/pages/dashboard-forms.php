<div class="dashboard principal">
	<div class="center">
		<div class="admin-center">
			<h1>Dashboard</h1>

			<div class="breadcrumb">
				<a href="dashboard">Início</a><i class="arrow-breadcrumb"></i><a href="<?php echo strtolower($arr['titulo']) ?>"><span><?php echo $arr['titulo'] ?></span></a><i class="arrow-breadcrumb"></i><span><?php echo $arr['titulo_form'] ?></span>
			</div>
			<div class="page-content">
				<?php include('templates/forms/form-'.$arr['form_type'].'.php') ?>
			</div>
		</div>
	</div>
</div>