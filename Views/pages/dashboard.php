<div class="dashboard principal">
	<div class="center">
		<div class="admin-center">
			<h1>Dashboard</h1>
			<div class="links-container grid">
				<div class="box-link">
					<a href="publicacoes">Publicações</a>
				</div>
				<div class="box-link">
					<a href="estados">Estados</a>
				</div>
				<div class="box-link">
					<a href="sliders">Slider</a>
				</div>
				<div class="box-link">
					<a href="usuarios">Usuários</a>
				</div>
			</div>
		</div>
	</div>
</div>