<div class="main-div <?php echo $arr['data_page'] ?>">
	<div class="banner-box">
		<div class="center">
			<h1><?php echo $arr['publicacoes'][0]['titulo'] ?></h1>
			<div class="box-video">
				<?php echo $arr['publicacoes'][0]['iframe_video'] ?>
			</div>
		</div>
	</div>
	<div class="box-autor">
		<div class="center">
			<span><?php echo $arr['publicacoes'][0]['usuario'] ?></span>
		</div>
	</div>
	<div class="conteudo-publicacao">
		<div class="center">
			<?php echo $arr['publicacoes'][0]['descricao'] ?>
		</div>
	</div>
</div>