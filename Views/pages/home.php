<div class="main-div">
	<?php if(!empty($arr['sliders'])){ ?>
	<div class="banner-box owl-carousel">
		<?php foreach($arr['sliders'] as $slider){?>
			<div class="banner-item" style="background: url('<?php echo public_path."uploads/".$slider['nome_imagem'] ?>') center;">
				<div class="center">
					<div class="text-banner">
						<p><?php echo $slider['texto_banner'] ?></p>
						<div class="box-button-video">
							<a href="#">
								<i class="button-video"></i>
								<span>Assistir vídeo</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
	<?php } ?>
	<div class="center">
		<div class="page-title">
			<h1>Viaje conosco pelo Brasil</h1>
			<p>O objetivo do blog é compartilhar as experiências vivenciadas nas viagens pelo Brasil.</p>
		</div>
		<div class="box-busca">
			<h2>Comece sua busca</h2>
			<div class="border-item"></div>
			<div class="wrapper-input">
				<input type="text" name="pesquisa" class="inputPesquisa" placeholder="Pra onde você quer ir?">
			</div>
		</div>
		<div class="viagens-estado">
			<h3>Viagens por Estado</h3>
		</div>
		<?php if(!empty($arr['estados'])){ ?>
		<div id="owl-carousel" class="box-estados owl-carousel">
		<a href="#" class="estados-item active" data-uf="0">Todos</a>
			<?php foreach($arr['estados'] as $estado) { ?>
				<a href="#" class="estados-item" data-uf="<?php echo $estado['id_estado'] ?>"><?php echo $estado['estado'] ?></a>
			<?php } ?>
		</div>
		<?php } ?>
		<div class="principais-viagens">
			<h3>Principais viagens</h3>
			<?php if(!empty($arr['publicacoes'])){ ?>
			<div class="box-viagens grid">
				<?php foreach($arr['publicacoes'] as $publicacao) { ?>
				<div class="box-viagens-item">
					<a href="publicacao?id=<?php echo $publicacao['id_publicacao'] ?>" class="pub-link">
						<h4><?php echo $publicacao['titulo'] ?></h4>
						<div class="border-item"></div>
					</a>
				</div>
				<?php }?>
			</div>
			<?php }else{ ?>
				<div class="box-viagens grid">
					<p>Ops, ainda não tem nenhuma publicação!</p>
				</div>
			<?php } ?>
		</div>
	</div>
</div>