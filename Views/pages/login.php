<div class="login principal">
	<div class="center">
		<div class="login-container">
			<div class="login-content">
				<a href="#" class="return-button"><i class="arrow-back"></i></a>
				<h1>Login</h1>
				<p>Não possui uma conta? <a href="#" class="cadastrar-usuario"> Cadastre-se</a></p>
				<form id="login_form">
					<div class="wrapper-input">
						<input type="text" name="email" placeholder="E-mail" class="campoObrigatorio vEmail">
						<i class="email-icon"></i>
					</div>
					<div class="wrapper-input">
						<input type="password" name="password" placeholder="Senha" class="campoObrigatorio">
						<i class="locker-icon"></i>
					</div>
					<button type="submit" class="btn-bg-blue btn-entrar">Entrar</button>
				</form>
				<a href="#" class="forgot-password">Esqueci minha senha</a>
			</div>
			<div class="logo">
				<a href="<?php echo root_path ?>"><i class="logo-icon"></i></a>
			</div>
		</div>
		<div class="box-msg">
			<span class="msg">dsjghsdaj</span>
		</div>
	</div>
</div>
<?php include('templates/modals/cadastrar-usuario-modal.php') ?>
<?php include('templates/modals/alterar-senha-modal.php') ?>