<div class="dashboard principal">
	<div class="center">
		<div class="admin-center">
			<h1>Dashboard</h1>
			<div class="breadcrumb">
				<a href="dashboard">Início</a><i class="arrow-breadcrumb"></i><a href=""></a><span><?php echo $arr['titulo'] ?></span>
			</div>
			<div class="page-content" data-page="<?php echo strtolower($arr['titulo']) ?>">
				<a href="cadastrar-usuarios" class="btn-adicionar btn-green">Adicionar novo</a>
				<table>
					<tr>
						<th>ID</th>
						<th>Usuário</th>
						<th>E-mail</th>
						<th>Ações</th>
					</tr>
					<?php if(isset($arr['usuarios'])){ ?>
						<?php foreach($arr['usuarios'] as $usuario){ ?> 
						<tr>
							<td style="width: 10%"><?php echo $usuario['id_usuario'] ?></td>
							<td style="width: 40%"><?php echo $usuario['usuario'] ?></td>
							<td style="width: 40%"><?php echo $usuario['email'] ?></td>
							<td style="width: 10%">
								<a href="#"><i class="btn-excluir" data-id="<?php echo $usuario['id_usuario'] ?>"></i></a>
								<a href="editar-usuarios?id=<?php echo $usuario['id_usuario'] ?>"><i class="btn-editar"></i></a>
							</td>
						</tr>
						<?php } ?>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
</div>
<?php include('templates/modals/exclude-modal.php') ?>