<div class="dashboard principal">
	<div class="center">
		<div class="admin-center">
			<h1>Dashboard</h1>

			<div class="breadcrumb">
				<a href="dashboard">Início</a><i class="arrow-breadcrumb"></i><a href=""></a><span><?php echo $arr['titulo'] ?></span>
			</div>
			<div class="page-content" data-page="<?php echo strtolower($arr['titulo']) ?>">
				<a href="cadastrar-publicacoes" class="btn-adicionar btn-green">Adicionar novo</a>
				<table>
					<tr>
						<th>ID</th>
						<th>Título</th>
						<th>Autor</th>
						<th>Ações</th>
					</tr>
					<?php if(isset($arr['publicacoes'])){ ?>
						<?php foreach($arr['publicacoes'] as $publicacao){ ?> 
						<tr>
							<td style="width: 10%"><?php echo $publicacao['id_publicacao'] ?></td>
							<td style="width: 60%"><?php echo $publicacao['titulo'] ?></td>
							<td style="width: 20%"><?php echo $publicacao['usuario'] ?></td>
							<td style="width: 10%">
								<a href="#"><i class="btn-excluir" data-id="<?php echo $publicacao['id_publicacao'] ?>"></i></a>
								<a href="editar-publicacoes?id=<?php echo $publicacao['id_publicacao'] ?>"><i class="btn-editar"></i></a>
							</td>
						</tr>
						<?php } ?>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
</div>
<?php include('templates/modals/exclude-modal.php') ?>