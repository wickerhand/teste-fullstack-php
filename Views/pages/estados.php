<div class="dashboard principal">
	<div class="center">
		<div class="admin-center">
			<h1>Dashboard</h1>

			<div class="breadcrumb">
				<a href="dashboard">Início</a><i class="arrow-breadcrumb"></i><a href=""></a><span><?php echo $arr['titulo'] ?></span>
			</div>
			<div class="page-content" data-page="<?php echo strtolower($arr['titulo']) ?>">
				<a href="cadastrar-estados" class="btn-adicionar btn-green">Adicionar novo</a>
				<table>
					<tr>
						<th>ID</th>
						<th>Estado</th>
						<th>UF</th>
						<th>Ações</th>
					</tr>
					<?php if(isset($arr['estados'])){ ?>
						<?php foreach($arr['estados'] as $estado){ ?> 
						<tr>
							<td style="width: 10%"><?php echo $estado['id_estado'] ?></td>
							<td style="width: 60%"><?php echo $estado['estado'] ?></td>
							<td style="width: 20%"><?php echo $estado['uf'] ?></td>
							<td style="width: 10%">
								<a href="#"><i class="btn-excluir" data-id="<?php echo $estado['id_estado'] ?>"></i></a>
								<a href="editar-estados?id=<?php echo $estado['id_estado'] ?>"><i class="btn-editar"></i></a>
							</td>
						</tr>
						<?php } ?>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
</div>
<?php include('templates/modals/exclude-modal.php') ?>