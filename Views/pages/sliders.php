<div class="dashboard principal">
	<div class="center">
		<div class="admin-center">
			<h1>Dashboard</h1>

			<div class="breadcrumb">
				<a href="dashboard">Início</a><i class="arrow-breadcrumb"></i><a href=""></a><span><?php echo $arr['titulo'] ?></span>
			</div>
			<div class="page-content" data-page="<?php echo strtolower($arr['titulo']) ?>">
				<a href="cadastrar-sliders" class="btn-adicionar btn-green">Adicionar novo</a>
				<table>
					<tr>
						<th>ID</th>
						<th>Título</th>
						<th>Ações</th>
					</tr>
					<?php if(isset($arr['sliders'])){ ?>
						<?php foreach($arr['sliders'] as $slider){ ?> 
						<tr>
							<td style="width: 10%"><?php echo $slider['id_slider'] ?></td>
							<td style="width: 80%"><?php echo $slider['titulo_slider'] ?></td>
							<td style="width: 10%">
								<a href="#"><i class="btn-excluir" data-id="<?php echo $slider['id_slider'] ?>"></i></a>
								<a href="editar-sliders?id=<?php echo $slider['id_slider'] ?>"><i class="btn-editar"></i></a>
							</td>
						</tr>
						<?php } ?>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
</div>
<?php include('templates/modals/exclude-modal.php') ?>