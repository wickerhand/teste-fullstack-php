<form id="form_<?php echo $arr['form_id'] ?>" data-form="<?php echo $arr['data_form'] ?>" data-main="<?php echo $arr['titulo'] ?>">
    <?php if(!isset($_GET['id'])){ ?>
    <input type="hidden" name='cadastrar' value="1">
    <?php }else{ ?>
    <input type="hidden" name='editar' value="1">
    <input type="hidden" name='id' value="<?php echo $_GET['id'] ?>">
    <?php } ?>
    <div class="wrapper-input">
        <input type="text" name="titulo" placeholder="Título" class="campoObrigatorio" value='<?php echo isset($arr['publicacao']) ? $arr['publicacao'][0]['titulo'] : "" ?>'>
    </div>
    <div class="wrapper-input">
        <select name="id_estado" class="campoObrigatorio">
            <option value="">Selecione um estado</option>
            <?php if(isset($arr['estados'])){ ?>
                <?php foreach($arr['estados'] as $estado) { ?>
                    <option <?php echo isset($arr['publicacao']) && $arr['publicacao'][0]['id_estado'] == $estado['id_estado'] ? 'selected' : '' ?> value="<?php echo $estado['id_estado'] ?>"><?php echo $estado['estado'] ?></option>
                <?php }?>
            <?php } ?>
        </select>
    </div>
    <div class="wrapper-input">
        <textarea type="text" name="iframe" placeholder="Insira o Iframe do vídeo" class="campoObrigatorio" ><?php echo isset($arr['publicacao']) ? strval($arr['publicacao'][0]['iframe_video']) : "" ?></textarea>
    </div>
    <textarea type="text" name="descricao" id="html_text" placeholder="Descrição" class="campoObrigatorio" ><?php echo isset($arr['publicacao']) ? strval($arr['publicacao'][0]['descricao']) : "" ?></textarea>
    <button class="btn-green btn-save">Salvar</button>
</form>
<div class="box-msg">
    <span class="msg"></span>
</div>