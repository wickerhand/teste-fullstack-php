<form id="form_<?php echo $arr['form_id'] ?>" data-form="<?php echo $arr['data_form'] ?>" data-main="<?php echo $arr['titulo'] ?>">
    <?php if(!isset($_GET['id'])){ ?>
    <input type="hidden" name='cadastrar' value="1">
    <?php }else{ ?>
    <input type="hidden" name='editar' value="1">
    <input type="hidden" name='id' value="<?php echo $_GET['id'] ?>">
    <?php } ?>
    <div class="wrapper-input">
        <input type="text" name="usuario" placeholder="Usuario" class="campoObrigatorio" value='<?php echo isset($arr['usuario']) ? $arr['usuario'][0]['usuario'] : "" ?>'>
    </div>
    <div class="wrapper-input">
        <input type="text" name="email" placeholder="E-mail" class="campoObrigatorio vEmail" value="<?php echo isset($arr['usuario']) ? $arr['usuario'][0]['email'] : "" ?>">
    </div>
    <?php if($arr['data_form'] == 'cadastrar-usuarios'){ ?>
    <div class="wrapper-input">
        <input type="password" name="senha" placeholder="Senha" class="campoObrigatorio" value="<?php echo isset($arr['usuario']) ? $arr['usuario'][0]['email'] : "" ?>">
    </div>
    <div class="wrapper-input">
        <input type="password" name="confirma_senha" placeholder="Confirme a senha" class="campoObrigatorio" value="<?php echo isset($arr['usuario']) ? $arr['usuario'][0]['email'] : "" ?>">
    </div>
    <?php }else{ ?>
        <div class="wrapper-input">
            <select name="status" class="campoObrigatorio">
                <?php if(isset($arr['usuario'])){ ?>
                    <option value="">Status</option>
                    <option <?php echo $arr['usuario'][0]['status'] == 'A' ? 'selected' : '' ?> value="A">Ativo</option>
                    <option <?php echo $arr['usuario'][0]['status'] == 'B' ? 'selected' : '' ?> value="B">Bloqueado</option>
                    <option <?php echo $arr['usuario'][0]['status'] == 'I' ? 'selected' : '' ?> value="I">Inativo</option>
                <?php } ?>
            </select>
        </div>
        <div class="box-alterar-senha right">
            <a href="#">Alterar senha</a>
        </div>
    <?php } ?>
    <button class="btn-green btn-save">Salvar</button>
</form>
<div class="box-msg">
    <span class="msg"></span>
</div>