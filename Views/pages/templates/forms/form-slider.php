<form id="form_<?php echo $arr['form_id'] ?>" data-form="<?php echo $arr['data_form'] ?>" data-main="<?php echo $arr['titulo'] ?>">
    <?php if(!isset($_GET['id'])){ ?>
    <input type="hidden" name='cadastrar' value="1">
    <?php }else{ ?>
    <input type="hidden" name='editar' value="1">
    <input type="hidden" name='id' value="<?php echo $_GET['id'] ?>">
    <?php } ?>
    <div class="wrapper-input">
        <input type="text" name="titulo_slider" placeholder="Título" class="campoObrigatorio" value='<?php echo isset($arr['slider']) ? $arr['slider'][0]['titulo_slider'] : "" ?>'>
    </div>
    <!--
    <div class="wrapper-input">
        <label for="foto" class="btn-green btn-upload">upload<i class="fas fa-upload"></i></label>
        <input type="file" name="foto" autocomplete="off" id="foto" class="campoObrigatorio inputFile">
        <span>Nome da imagem</span>
    </div>
    -->
    <button class="btn-green btn-save">Salvar</button>
</form>
<?php if($arr['data_form'] == 'editar-sliders'){ ?>
<div class="formulario-anexo">
    <p>Cadastrar imagens</p>
    <form id="form_anexo">
        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
        <div class="wrapper-input">
            <input type="text" name="texto_banner" placeholder="Título" class="campoObrigatorio">
        </div>
        <div class="wrapper-input">
            <label for="foto" class="btn-green btn-upload">upload</label>
            <input type="file" name="foto" autocomplete="off" id="foto" class="campoObrigatorio inputFile">
            <span>Nome da imagem</span>
        </div>
        <button class="btn-green btn-save">Salvar</button>
    </form>
</div>
<table id="table_anexo">
    <tr>
        <th>Nome da imagem</th>
        <th>Ações</th>
    </tr>
    <?php if(!empty($arr['imagens_slider'])){ ?>
        <?php foreach($arr['imagens_slider'] as $slider){ ?> 
            <tr>
                <td style="width: 90%"><?php echo $slider['nome_imagem'] ?></td>
                <td style="width: 10%">
                    <a href="#"><i class="btn-excluir btn-excluir-anexo" data-id="<?php echo $slider['id_imagem_slider'] ?>"></i></a>
                </td>
            </tr>
        <?php } ?>
    <?php } ?>
</table>
<?php } ?>
<div class="box-msg">
    <span class="msg"></span>
</div>