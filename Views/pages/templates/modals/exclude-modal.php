<div id="exclude-modal" class="exclude-modal">
    <h2>Deseja realmente excluir esse item?</h2>
    <a href="#" class="btn-exclude btn-modal btn-green" data-id="">Excluir</a>
    <a href="#" class="btn-cancel btn-modal btn-red" rel="modal:close">Cancelar</a>
</div>