<div id="alterar-senha-modal" class="alterar-senha-modal modal">
    <h2>Cadastre sua conta</h2>
    <form id="alterar_senha">
        <input type="hidden" name="alterar_senha" value="1">
        <div class="wrapper-input">
            <input type="text" name="email" placeholder="E-mail" class="campoObrigatorio vEmail">
        </div>
        <div class="wrapper-input">
            <input type="password" name="senha" placeholder="Senha" class="campoObrigatorio">
        </div>
        <div class="wrapper-input">
            <input type="password" name="confirma_senha" placeholder="Confirmar senha" class="campoObrigatorio">
        </div>
        <button type="submit" class="btn-bg-blue btn-entrar">Entrar</button>
    </form>
</div>