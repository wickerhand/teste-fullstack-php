<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo self::titulo.' | '.$arr['titulo']; ?></title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
	<link href="<?php echo public_path ?>frontend/css/style.css" rel="stylesheet" type="text/css">
	<link href="<?php echo public_path ?>backend/plugins/trumbowyg/dist/ui/trumbowyg.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo public_path ?>common/css/jquery.modal.css" rel="stylesheet" type="text/css">
	<link href="<?php echo public_path ?>common/css/owl.carousel.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo public_path ?>common/css/jquery-ui.css" rel="stylesheet" type="text/css">
	
	<script src="<?php echo public_path ?>common/js/jquery-3.3.1.min.js" type="text/javascript"></script>
	<script src="<?php echo public_path ?>common/js/owl.carousel.min.js" type="text/javascript"></script>
	<script src="<?php echo public_path ?>backend/plugins/trumbowyg/dist/trumbowyg.min.js" type="text/javascript"></script>
	<script src="<?php echo public_path ?>frontend/js/scripts.js" type="text/javascript"></script>
	<script src="<?php echo public_path ?>common/js/jquery.modal.js" type="text/javascript"></script>
	<script src="<?php echo public_path ?>common/js/jquery-ui.js" type="text/javascript"></script>
</head>
<body>
<?php if($arr['titulo'] != 'Login'){ ?>
<header>
	<div class="center">
		<div class="logo">
			<a href="<?php echo root_path ?>"><i class="logo-icon"></i></a>
		</div>
		<nav class="menu">
			<a href="<?php echo root_path ?>">Home</a>
			<?php if(isset($_SESSION['logado'])){ ?>
			<a href="#" class="btn-login btn-bg-blue btn-logout">Sair</a>
			<?php }else{ ?>
			<a href="login" class="btn-login btn-bg-blue">Login</a>
			<?php }?>
		</nav>
		<div class="clear"></div>
	</div>
</header>
<?php } ?>